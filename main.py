from telegram.ext import Updater, CommandHandler, Filters
import settings
import datetime
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google_auth_oauthlib.flow import Flow
from google.auth.transport.requests import Request

def start(bot, update):
    text = 'Чтобы зарегистрировать календарь, с которым вы хотите взаимодействовать, используйте команду /calendar'
    update.message.reply_text(text)

def calendar_auth(bot, update):

    flow = Flow.from_client_secrets_file(
                            'client_id.json',
                            scopes=['profile', 'email'],
                            redirect_uri='urn:ietf:wg:oauth:2.0:oob')
    auth_url = flow.authorization_url(prompt='consent')
    update.message.reply_text(auth_url)
    print(auth_url)

def main():
    telegram_bot_token = settings.TELEGRAM_TOKEN_BOT
    mybot = Updater(telegram_bot_token)
    dp = mybot.dispatcher
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("calendar", calendar_auth))

    mybot.start_polling()
    mybot.idle()


if __name__ == "__main__":
    main()